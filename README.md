# interChat
 This is my real-time chatting application that was created using react, node.js and socket.io. 

**How to build/run/compile project:**

Once you have downloaded the project via the gitlab ssh key (master branch), please navigate into the folder structure of the project and familiarise yourself with it. You will see 2 folders, ‘client’ and ‘server’ you will need to run both.

cmd into server:
Run npm start

cmd into client:
Run npm start	

Navigate to http://localhost:3000/ on your internet browser 
You will need npm installed on your machine you are running this from.



**Details of any assumptions you have made in design or development:**

An assumption that I made during the design of this application revolves around closing the application. It was mentioned in the MVP about being able to stop the interaction, and the way I have designed my application to fulfil this feature is through a simple close tab button where the user can exit/leave the chat by clicking on the ‘x’ on the info bar of  the chat this is implemented in my InfoBar component. Once that is clicked the user would be directed back to the main join page of the application where they would be prompted to enter their name and room again to start chatting.
Another assumption that was made during the design of the application was to do with providing a leader board or a list of collaborators in the virtual community as stated in the MVP under the additional requirements. I interpreted this as a list of active users in the chat, therefore I implemented a way that shows this in my Text component. 
Additionally, another assumption that was made revolved around the application allowing interactions to be associated with a particular player/collaborator. I assumed in the design of this chatting application that this would be the people that are chatting being associated with a name. Users would be prompted to enter their name and room as these 2 credentials are required to use the chatting application. 



**Explanation of some of the key paradigms used within your code (added as GitLab snippets):**

One of the main programming paradigms used in this application was Functional. I believe using this paradigm was effective as its ability to not show any side-effects in the code was hugely beneficial when implementing the useEffect hook in Chat.js (See Gitlab snippets ‘React hooks’). It reduces the likelihood that the code will introduce bugs since it’s not going to mutate and the bug is easy to spot as it’s within the scope of the function. Once a variable has a value assigned to it, it is no longer subject to change.
Functional Programming has always been straightforward so it’s easy to spot some inconsistencies and debug some bugs in the function. Constructing functions are cleaner and easier to maintain than constructing a class as in OOP since you need to think in terms of imperative/procedural pattern such as designing class hierarchies, encapsulation, inheritance, methods, polymorphism. 



**Examples of conventions applied:**

During development of this application both functional and class component were utilised. Functional components were used throughout development of the application e.g when implementing the Insert component, I believe that using functional components was the most appropriate method for implementing the insert message bar as the isn’t no logic required for this component, it simple accepts the props in function and returns html. As you can see message, setMessage and sendMessage where passed as props and then used in the function.                       (See Gitlab snippets ‘Functional component’)
I used React hooks in my Chat.js component, the useEffect() hook is used to replicate lifecycle behaviour, and useState is used to store state in a functional component. This useEffect hook retrieves the data that the user enters this being the users name and the room and displays this in the url using location.search, it is also emitting to the server. ENDPOINT and location.search is passed through is an array and only if these values change this will re-render the use effect. ENDPOINT being localhost:5000. (See Gitlab snippets ‘React hooks’). 
Overall I think using functional components throughout my application was the best approach for the overall architecture for my program because it has made it easier to separate container and presentational components because I needed to think more about my component’s state if I don’t have access to setState() in my component. As well as that functional components are much easier to read and test as there is no logic required. 



**Reflection and justification on the technology used for this project in relation to alternatives:**

The technology that was used during the implementation of this application covers what was focused on in lessons, this being React for the frontend and Node.js and socet.io web socket library for the backend. I found using these 2 technologies to programme this application very simple and straight forward as there was numerous of online resources that guided development. Alternatively, firebase and mongodb could have been used for the backend server of this application however as I was already familiar with node.js and socket.io I went with implementing what I was more comfortable in using. I believe using node.js and socket.io for the backend proved to be very effective as the web socket library allowed messages to be sent in real-time over the internet. Reflecting back on the design and development of the application I believe I could have implemented the CSS and styling of the app in a better way, this could be through using bootstrap however, I believe if I was to take this app to the next level and implement more pages and features that bootstrap would prove to be more useful as using Bootstrap just to simply add one heading or button is not a good enough reason to use it in the first place.


