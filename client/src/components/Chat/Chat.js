import React, { useState, useEffect } from "react";
import queryString from 'query-string';
import io from "socket.io-client";

import './Chat.css';
import InfoBar from "../InfoBar/InfoBar";
import Insert from "../Insert/Insert";
import Messages from "../Messages/Messages";
import Text from'../Text/Text';


let socket;

const  Chat = ({ location }) => {
    const [name, setName] = useState('');
    const [room, setRoom] = useState('');
    const [users, setUsers] = useState('');
    const [lastJoin, setLastJoin] = useState('');
    const [message, setMessage] = useState('');
    const [messages, setMessages] = useState([]);
    const ENDPOINT = 'localhost:5000';

    useEffect(() => {
        const {name, room} = queryString.parse(location.search);

        socket = io(ENDPOINT);

        setName(name);
        setRoom(room);

        socket.emit('join', { name, room }, (error) => {
            if(error) {
                alert(error);
            }
        });
    }, [ENDPOINT, location.search]);

    useEffect(() => {
        socket.on('message', message => {
            setMessages(messages => [ ...messages, message ]);
        });

        //Getting relevant information to display on chat screen!
        socket.on("roomData", ({ users, lastJoin }) => {
            setUsers(users);
            setLastJoin(lastJoin)
        });
    }, []);

    const sendMessage = (event) => {
        event.preventDefault();
        if(message) {
            socket.emit('sendMessage', message, () => setMessage(''));
        }
    }

    return (
        <div className="outerContainer">
            <div className="container">
                <InfoBar room={room} />
                <Messages messages={messages} name={name} />
                <Insert message={message} setMessage={setMessage} sendMessage={sendMessage} />
            </div>
            <Text messages={messages} lastJoin={lastJoin} users={users}/>
        </div>
    );
}

export default Chat;