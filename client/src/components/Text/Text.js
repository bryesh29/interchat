import React from 'react';

import onlineImg from '../../img/onlineImg.png';
import './Text.css'
import interchat from "../../img/interchat.png";


const Text = ({users, messages, lastJoin, lastJoinName})=>(
    <div className = "text">
        <div>
            <h1> Welcome to interChat! <a href="/"><img src={interchat} className="chatImage" alt="interchat img"/></a></h1>
            <h2>Once you have entered your name and joined a room,
                please insert message to start chatting!
            </h2>
            <h2>If you would like to leave the chat and close the application,
                please click the 'x' on the top right corner of chat box</h2>
            <h2>Try it out right now! </h2>

        </div>
        {
            users
                ? (
                    <div>
                        <h1>Active in chat:</h1>
                        <div className="activeContainer d-flex">
                                {users.map(({name}) => (
                                    <div key={name} className="activeItem">
                                        <img className="onlineImg" alt="Online Img" src={onlineImg}/>
                                        <h2>{name}</h2>
                                    </div>
                                ))}
                        </div>
                    </div>
                )
                : null
        }
        <div>
            <h1>Chat Information:</h1>
            <div className="activeContainer">
                <div className="activeItem">
                    <h2>{messages.length} Message(s) sent</h2>
                </div>
                <div className="activeItem">
                    <h2>{lastJoin}</h2>
                </div>
            </div>
        </div>
    </div>
);
export default Text;