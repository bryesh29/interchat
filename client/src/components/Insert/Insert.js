import React from 'react';

import './Insert.css';

const Insert = ({message, setMessage, sendMessage}) => (
    <form className="form">
        <input
            className="insert"
            type="text"
            placeholder="Insert a message..."
            value={message}
            onChange={({ target: { value } }) => setMessage(value)}
            onKeyPress={event => event.key === 'Enter' ? sendMessage(event) : null}
        />
        <button className="sendButton" onClick={e => sendMessage(e)}>Send</button>
    </form>
)

export default Insert;
