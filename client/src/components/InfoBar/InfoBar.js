import React from 'react';
import './InfoBar.css';

import onlineImg from '../../img/onlineImg.png';
import closeImg from '../../img/closeImg.png';

const InfoBar = ({ room }) => (
    <div className="infoBar">
        <div className="leftInnerContainer">
            <img className="onlineImg" src={onlineImg} alt="online img" />
            <h3>{room}</h3>
        </div>
        <div className="rightInnerContainer">
            <a href="/"><img src={closeImg} alt="close img" /></a>
        </div>
    </div>
);

export default InfoBar;