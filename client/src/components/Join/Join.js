import React, { useState } from 'react';
import { Link } from "react-router-dom";
import './Join.css';
import interchat from '../../img/interchat.png';

export default function SignIn() {

        const [name, setName] = useState('');
        const [room, setRoom] = useState('');

        return (
            <div className="joinOuterContainer">
                <div className="joinInnerContainer">
                    <h1 className="heading">interChat</h1>
                    <a href="/"><img src={interchat} alt="interchat img"/></a>
                    <h2>Please enter room and name to start chatting!</h2>
                    <div><input placeholder="Name" className="joinInput" type="text"
                                onChange={(event) => setName(event.target.value)}/></div>
                    <div><input placeholder="Room" className="joinInput mt-20" type="text"
                                onChange={(event) => setRoom(event.target.value)}/></div>
                    <Link onClick={event => (!name || !room) ? event.preventDefault() : null}
                          to={`/chat?name=${name}&room=${room}`}>
                        <button className="button mt-20" type="submit">Sign In</button>

                    </Link>
                </div>
            </div>


        );
}
