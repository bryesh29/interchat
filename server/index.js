const express = require('express');
const socketio = require('socket.io');
const http = require('http');

const { addUser, removeUser, getUser, getUsersInRoom } = require('./users');

const PORT = process.env.PORT || 5000;

const router = require('./router');

const app = express();
const server = http.createServer(app);

//This is how we stop the CORS error happening and allow an array of allowed origins,
// such as localhost (127.0.0.1)
const options={
    cors:true,
    origins:["http://127.0.0.1:3000"],
}
const io = socketio(server, options);

io.on('connect', (socket) => {
    socket.on('join', ({ name, room }, callback) => {
        const { error, user } = addUser({ id: socket.id, name, room });
        if(error) return callback(error);
        socket.join(user.room);
        socket.emit('message', { user: 'admin', text: `${user.name}, welcome to room ${user.room}.`, date: getDate()});
        socket.broadcast.to(user.room).emit('message', { user: 'admin', text: `${user.name} has joined!`, date: getDate() });
        io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room), lastJoin: user.name + " joined most recently at " + getDate()});
        callback();
    });

    socket.on('sendMessage', (message, callback) =>{
        const user = getUser(socket.id);
        io.to(user.room).emit('message', {user:user.name, text:message, date: getDate()});
        callback();
    });

    socket.on('disconnect', () => {
        const user = removeUser(socket.id);
        if (user) {
            io.to(user.room).emit('message', {user: 'Admin', text: `${user.name} has left.`});
            io.to(user.room).emit('roomData', {room: user.room, users: getUsersInRoom(user.room)});
        }
    })
});

app.use(router);

server.listen(PORT, () => console.log('Server has started}'));

function getDate(){
    const d = new Date;
    return [d.getMonth() + 1,
            d.getDate(),
            d.getFullYear()].join('/') + ' ' +
        [d.getHours(),
            d.getMinutes(),
            d.getSeconds()].join(':')
}